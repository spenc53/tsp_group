﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSP
{
    class State
    {

        //  space Complexity per State =    3n + n^2 + 1 = n^2
        List<City> visited;             //  n
        List<City> notVisited;          //  n
        Dictionary<City, int> cityMap;  //  n
        double[,] distanceGraph;        //  n^2
        double bound;                   // 1

        public List<City> getNotVisited()
        {
            return notVisited;
        }

        public List<City> getVisited()
        {
            return visited;
        }

        public double getBound()
        {
            return bound;
        }

        public double[,] getMatrix()
        {
            return distanceGraph;
        }

        public State(double[,] distanceGraph, Dictionary<City, int> cityMap, List<City> visited, List<City> notVisited, City toVisit, double bound)
        {

            //copy over data
            this.cityMap = cityMap;
            this.visited = new List<City>(visited);
            this.notVisited = new List<City>(notVisited);
            this.bound = bound;
            int maxLength = cityMap.Count;
            this.distanceGraph = new double[maxLength,maxLength];
            int size = sizeof(double);
            int length = maxLength * maxLength * size;
            this.distanceGraph = distanceGraph.Clone() as double[,];
            
            // if it came from a node, then add it distance and set it's distance to infinite
            if (visited.Count > 0)
            {
                //vist the node toVisit
                int f = cityMap[this.visited[visited.Count - 1]];
                int t = cityMap[toVisit];
                this.bound += this.distanceGraph[t, f];

                //can no longer visit that node
                for (int row = 0; row < maxLength; row++)
                {
                    this.distanceGraph[t, row] = double.PositiveInfinity;
                    this.distanceGraph[row, f] = double.PositiveInfinity;
                }
            }

            //if (double.IsPositiveInfinity(this.bound)) return;
            this.visited.Add(toVisit);
            this.notVisited.Remove(toVisit);

            //if visited all the nodes, return because there is no use setting everything else to infinity
            if(this.notVisited.Count == 0)
            {
                return;
            }


            //calculation for this is at worst(n^2) aka double for loop
            //reduce matrix rows and update bounds
            for (int row = 0; row < maxLength; row++)
            {
                double min = double.PositiveInfinity;
                for (int col = 0; col < maxLength; col++)
                {
                    if (this.distanceGraph[row, col] < min)
                    {
                        min = this.distanceGraph[row, col];
                        if (min == 0) continue;
                    }
                }
                if (double.IsPositiveInfinity(min)) continue;
                if (min == 0) continue;

                this.bound += min;

                for (int col = 0; col < maxLength; col++)
                {
                    this.distanceGraph[row, col] -= min;
                    
                }
            }

            //calculation for this is at worst(n^2) aka double for loop
            //reduce matrix columns and update bounds
            for (int col = 0; col < maxLength; col++)
            {
                double min = double.PositiveInfinity;
                for (int row = 0; row < maxLength; row++)
                {
                    if (this.distanceGraph[row, col] < min)
                    {
                        min = this.distanceGraph[row, col];
                        if (min == 0) continue;
                    }
                }
                if (double.IsPositiveInfinity(min)) continue;
                

                this.bound += min;

                for (int row = 0; row < maxLength; row++)
                {
                    this.distanceGraph[row, col] -= min;
                }
            }
        }

        public State(double[,] distanceGraph, Dictionary<City, int> cityMap, List<City> visited, List<City> notVisited, double bound)
        {
            //initalizer of the first 
            this.cityMap = cityMap;
            this.visited = new List<City>(visited);
            this.notVisited = new List<City>(notVisited);
            this.bound = bound;

           
            this.distanceGraph = distanceGraph.Clone() as double[,];
        }

        public double value()
        {
            // After messing with some different ways to calculate a value,
            // I settled on how many were left to visit. This way it does depth first and,
            // if it finds a path that is shorter faster, it will choose that path and will help with pruning.
            if (visited.Count == 0) return notVisited.Count;
            return bound / visited.Count;
            //return bound;
        }

        public static double[,] makeGraph(Dictionary<City, int> cityMap)
        {
            //Time Complexity is O(n^2)
            //this method is used to initalize a matrix that shows distance to cities from another city   
            double[,] distanceMatrix = new double[cityMap.Keys.Count, cityMap.Keys.Count];
            //return distance matrix thing
            foreach (City c1 in cityMap.Keys)
            {
                int from = cityMap[c1];
                foreach(City c2 in cityMap.Keys)
                {
                    int to = cityMap[c2];
                    if(from == to)
                    {
                        distanceMatrix[from,to] = double.PositiveInfinity;
                        continue;
                    }
                    distanceMatrix[from, to] = c1.costToGetTo(c2);
                }
            }

            return distanceMatrix;
        }

    }
}
