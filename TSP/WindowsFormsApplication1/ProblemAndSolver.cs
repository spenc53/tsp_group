using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Diagnostics;


namespace TSP
{

    class ProblemAndSolver
    {

        private class TSPSolution
        {
            /// <summary>
            /// we use the representation [cityB,cityA,cityC] 
            /// to mean that cityB is the first city in the solution, cityA is the second, cityC is the third 
            /// and the edge from cityC to cityB is the final edge in the path.  
            /// You are, of course, free to use a different representation if it would be more convenient or efficient 
            /// for your data structure(s) and search algorithm. 
            /// </summary>
            public ArrayList
                Route;

            /// <summary>
            /// constructor
            /// </summary>
            /// <param name="iroute">a (hopefully) valid tour</param>
            public TSPSolution(ArrayList iroute)
            {
                Route = new ArrayList(iroute);
            }

            /// <summary>
            /// Compute the cost of the current route.  
            /// Note: This does not check that the route is complete.
            /// It assumes that the route passes from the last city back to the first city. 
            /// </summary>
            /// <returns></returns>
            public double costOfRoute()
            {
                // go through each edge in the route and add up the cost. 
                int x;
                City here;
                double cost = 0D;

                for (x = 0; x < Route.Count - 1; x++)
                {
                    here = Route[x] as City;
                    cost += here.costToGetTo(Route[x + 1] as City);
                }

                // go from the last city to the first. 
                here = Route[Route.Count - 1] as City;
                cost += here.costToGetTo(Route[0] as City);
                return cost;
            }
        }

        #region Private members 

        /// <summary>
        /// Default number of cities (unused -- to set defaults, change the values in the GUI form)
        /// </summary>
        // (This is no longer used -- to set default values, edit the form directly.  Open Form1.cs,
        // click on the Problem Size text box, go to the Properties window (lower right corner), 
        // and change the "Text" value.)
        private const int DEFAULT_SIZE = 25;

        /// <summary>
        /// Default time limit (unused -- to set defaults, change the values in the GUI form)
        /// </summary>
        // (This is no longer used -- to set default values, edit the form directly.  Open Form1.cs,
        // click on the Time text box, go to the Properties window (lower right corner), 
        // and change the "Text" value.)
        private const int TIME_LIMIT = 60;        //in seconds

        private const int CITY_ICON_SIZE = 5;


        // For normal and hard modes:
        // hard mode only
        private const double FRACTION_OF_PATHS_TO_REMOVE = 0.20;

        /// <summary>
        /// the cities in the current problem.
        /// </summary>
        private City[] Cities;
        /// <summary>
        /// a route through the current problem, useful as a temporary variable. 
        /// </summary>
        private ArrayList Route;
        /// <summary>
        /// best solution so far. 
        /// </summary>
        private TSPSolution bssf; 

        /// <summary>
        /// how to color various things. 
        /// </summary>
        private Brush cityBrushStartStyle;
        private Brush cityBrushStyle;
        private Pen routePenStyle;


        /// <summary>
        /// keep track of the seed value so that the same sequence of problems can be 
        /// regenerated next time the generator is run. 
        /// </summary>
        private int _seed;
        /// <summary>
        /// number of cities to include in a problem. 
        /// </summary>
        private int _size;

        /// <summary>
        /// Difficulty level
        /// </summary>
        private HardMode.Modes _mode;

        /// <summary>
        /// random number generator. 
        /// </summary>
        private Random rnd;

        /// <summary>
        /// time limit in milliseconds for state space search
        /// can be used by any solver method to truncate the search and return the BSSF
        /// </summary>
        private int time_limit;
        #endregion

        #region Public members

        /// <summary>
        /// These three constants are used for convenience/clarity in populating and accessing the results array that is passed back to the calling Form
        /// </summary>
        public const int COST = 0;           
        public const int TIME = 1;
        public const int COUNT = 2;
        
        public int Size
        {
            get { return _size; }
        }

        public int Seed
        {
            get { return _seed; }
        }
        #endregion

        #region Constructors
        public ProblemAndSolver()
        {
            this._seed = 1; 
            rnd = new Random(1);
            this._size = DEFAULT_SIZE;
            this.time_limit = TIME_LIMIT * 1000;                  // TIME_LIMIT is in seconds, but timer wants it in milliseconds

            this.resetData();
        }

        public ProblemAndSolver(int seed)
        {
            this._seed = seed;
            rnd = new Random(seed);
            this._size = DEFAULT_SIZE;
            this.time_limit = TIME_LIMIT * 1000;                  // TIME_LIMIT is in seconds, but timer wants it in milliseconds

            this.resetData();
        }

        public ProblemAndSolver(int seed, int size)
        {
            this._seed = seed;
            this._size = size;
            rnd = new Random(seed);
            this.time_limit = TIME_LIMIT * 1000;                        // TIME_LIMIT is in seconds, but timer wants it in milliseconds

            this.resetData();
        }
        public ProblemAndSolver(int seed, int size, int time)
        {
            this._seed = seed;
            this._size = size;
            rnd = new Random(seed);
            this.time_limit = time*1000;                        // time is entered in the GUI in seconds, but timer wants it in milliseconds

            this.resetData();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Reset the problem instance.
        /// </summary>
        private void resetData()
        {

            Cities = new City[_size];
            Route = new ArrayList(_size);
            bssf = null;

            if (_mode == HardMode.Modes.Easy)
            {
                for (int i = 0; i < _size; i++)
                    Cities[i] = new City(rnd.NextDouble(), rnd.NextDouble());
            }
            else // Medium and hard
            {
                for (int i = 0; i < _size; i++)
                    Cities[i] = new City(rnd.NextDouble(), rnd.NextDouble(), rnd.NextDouble() * City.MAX_ELEVATION);
            }

            HardMode mm = new HardMode(this._mode, this.rnd, Cities);
            if (_mode == HardMode.Modes.Hard)
            {
                int edgesToRemove = (int)(_size * FRACTION_OF_PATHS_TO_REMOVE);
                mm.removePaths(edgesToRemove);
            }
            City.setModeManager(mm);

            cityBrushStyle = new SolidBrush(Color.Black);
            cityBrushStartStyle = new SolidBrush(Color.Red);
            routePenStyle = new Pen(Color.Blue,1);
            routePenStyle.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// make a new problem with the given size.
        /// </summary>
        /// <param name="size">number of cities</param>
        public void GenerateProblem(int size, HardMode.Modes mode)
        {
            this._size = size;
            this._mode = mode;
            resetData();
        }

        /// <summary>
        /// make a new problem with the given size, now including timelimit paremeter that was added to form.
        /// </summary>
        /// <param name="size">number of cities</param>
        public void GenerateProblem(int size, HardMode.Modes mode, int timelimit)
        {
            this._size = size;
            this._mode = mode;
            this.time_limit = timelimit*1000;                                   //convert seconds to milliseconds
            resetData();
        }

        /// <summary>
        /// return a copy of the cities in this problem. 
        /// </summary>
        /// <returns>array of cities</returns>
        public City[] GetCities()
        {
            City[] retCities = new City[Cities.Length];
            Array.Copy(Cities, retCities, Cities.Length);
            return retCities;
        }

        /// <summary>
        /// draw the cities in the problem.  if the bssf member is defined, then
        /// draw that too. 
        /// </summary>
        /// <param name="g">where to draw the stuff</param>
        public void Draw(Graphics g)
        {
            float width  = g.VisibleClipBounds.Width-45F;
            float height = g.VisibleClipBounds.Height-45F;
            Font labelFont = new Font("Arial", 10);

            // Draw lines
            if (bssf != null)
            {
                // make a list of points. 
                Point[] ps = new Point[bssf.Route.Count];
                int index = 0;
                foreach (City c in bssf.Route)
                {
                    if (index < bssf.Route.Count -1)
                        g.DrawString(" " + index +"("+c.costToGetTo(bssf.Route[index+1]as City)+")", labelFont, cityBrushStartStyle, new PointF((float)c.X * width + 3F, (float)c.Y * height));
                    else 
                        g.DrawString(" " + index +"("+c.costToGetTo(bssf.Route[0]as City)+")", labelFont, cityBrushStartStyle, new PointF((float)c.X * width + 3F, (float)c.Y * height));
                    ps[index++] = new Point((int)(c.X * width) + CITY_ICON_SIZE / 2, (int)(c.Y * height) + CITY_ICON_SIZE / 2);
                }

                if (ps.Length > 0)
                {
                    g.DrawLines(routePenStyle, ps);
                    g.FillEllipse(cityBrushStartStyle, (float)Cities[0].X * width - 1, (float)Cities[0].Y * height - 1, CITY_ICON_SIZE + 2, CITY_ICON_SIZE + 2);
                }

                // draw the last line. 
                g.DrawLine(routePenStyle, ps[0], ps[ps.Length - 1]);
            }

            // Draw city dots
            foreach (City c in Cities)
            {
                g.FillEllipse(cityBrushStyle, (float)c.X * width, (float)c.Y * height, CITY_ICON_SIZE, CITY_ICON_SIZE);
            }

        }

        /// <summary>
        ///  return the cost of the best solution so far. 
        /// </summary>
        /// <returns></returns>
        public double costOfBssf ()
        {
            if (bssf != null)
                return (bssf.costOfRoute());
            else
                return -1D; 
        }

        /// <summary>
        /// This is the entry point for the default solver
        /// which just finds a valid random tour 
        /// </summary>
        /// <returns>results array for GUI that contains three ints: cost of solution, time spent to find solution, number of solutions found during search (not counting initial BSSF estimate)</returns>
        public string[] defaultSolveProblem()
        {
            int i, swap, temp, count=0;
            string[] results = new string[3];
            int[] perm = new int[Cities.Length];
            Route = new ArrayList();
            Random rnd = new Random();
            Stopwatch timer = new Stopwatch();

            timer.Start();

            do
            {
                for (i = 0; i < perm.Length; i++)                                 // create a random permutation template
                    perm[i] = i;
                for (i = 0; i < perm.Length; i++)
                {
                    swap = i;
                    while (swap == i)
                        swap = rnd.Next(0, Cities.Length);
                    temp = perm[i];
                    perm[i] = perm[swap];
                    perm[swap] = temp;
                }
                Route.Clear();
                for (i = 0; i < Cities.Length; i++)                            // Now build the route using the random permutation 
                {
                    Route.Add(Cities[perm[i]]);
                }
                bssf = new TSPSolution(Route);
                count++;
            } while (costOfBssf() == double.PositiveInfinity);                // until a valid route is found
            timer.Stop();

            results[COST] = costOfBssf().ToString();                          // load results array
            results[TIME] = timer.Elapsed.ToString();
            results[COUNT] = count.ToString();

            return results;
        }

        /// <summary>
        /// performs a Branch and Bound search of the state space of partial tours
        /// stops when time limit expires and uses BSSF as solution
        /// </summary>
        /// <returns>results array for GUI that contains three ints: cost of solution, time spent to find solution, number of solutions found during search (not counting initial BSSF estimate)</returns>
        public string[] bBSolveProblem()
        {
            string[] results = new string[3];
            Stopwatch timer = new Stopwatch();



            timer.Start();

            // TODO: Add your implementation for a greedy solver here.
            ArrayList CitiesToVisit = new ArrayList();
            Dictionary<City, int> citiesMap = new Dictionary<City, int>();
            List<City> notVisited = new List<City>();

            //This loop is used to give cities an number associated with it
            //Time Complexity O(n)
            for (int i = 0; i < Cities.Length; i++)
            {
                citiesMap.Add(Cities[i], i);
                notVisited.Add(Cities[i]);
            }


            // initial best cost and solution (greedy algorithm was used)
            greedySolveProblem();
            double bestCost = costOfBssf();
            Route = bssf.Route;


            ArrayPriorityQueue queue = new ArrayPriorityQueue();
            // add inital state to queue
            State inital = new State(State.makeGraph(citiesMap), citiesMap, new List<City>(), notVisited, 0);
            queue.insert(inital);

            int solutionsFound = 0;
            int states_created = 1;
            int pruned = 0;
            int max_queue = 0;

            //generate children and add to queue

            while (queue.size() > 0)
            {
                if (max_queue < queue.size()) max_queue = queue.size();


                State u = queue.deleteMin();
                if (u.getBound() >= bestCost)
                {
                    pruned++;
                    continue;
                }
                //aka all in the notvisited list
                foreach (City c in u.getNotVisited())
                {

                    State state = new State(u.getMatrix(), citiesMap, u.getVisited(), u.getNotVisited(), c, u.getBound());
                    states_created++;
                    if (state.getBound() < bestCost)
                    {
                        if (state.getNotVisited().Count == 0)
                        {
                            solutionsFound += 1;

                            Route = new ArrayList();

                            foreach (City city in state.getVisited())
                            {
                                Route.Add(city);
                            }

                            bssf = new TSPSolution(Route);
                            //bestCost = bssf.costOfRoute();
                            bestCost = state.getBound();
                        }
                        else
                        {
                            queue.insert(state);
                        }
                    }
                    else
                    {
                        pruned++;
                    }


                    if (timer.Elapsed.TotalMilliseconds >= time_limit)
                    {
                        break;
                    }

                }
                if (timer.Elapsed.TotalSeconds >= time_limit)
                {
                    break;
                }

            }


            bssf = new TSPSolution(Route);
            timer.Stop();
            results[COST] = costOfBssf().ToString();    // load results into array here, replacing these dummy values
            results[TIME] = timer.Elapsed.ToString();
            results[COUNT] = solutionsFound.ToString();

            Console.WriteLine("Solutions: " + solutionsFound.ToString());
            Console.WriteLine("States Created: " + states_created.ToString());
            Console.WriteLine("Pruned: " + pruned.ToString());
            Console.WriteLine("Max_queue : " + max_queue.ToString());

            return results;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        // These additional solver methods will be implemented as part of the group project.
        ////////////////////////////////////////////////////////////////////////////////////////////

        public void recursiveGreedySolver(List<City> notVisited, List<City> visited)
        {
            if (notVisited.Count == 0)
            {
                City f = visited[visited.Count - 1];
                City t = visited[0];
                if (double.IsPositiveInfinity(f.costToGetTo(t)))
                {
                    return;
                }

                Route = new ArrayList();

                foreach (City city in visited)
                {
                    Route.Add(city);
                }
                bssf = new TSPSolution(Route);
                return;
            }

            //at least 1 should be in visited
            City from = visited[visited.Count - 1];
            List<City> toVisit = new List<City>(notVisited);
            toVisit.Sort((c1, c2) => from.costToGetTo(c1).CompareTo(from.costToGetTo(c2)));

            notVisited.Remove(toVisit[0]);
            visited.Add(toVisit[0]);
            recursiveGreedySolver(notVisited, visited);



        }

        /// <summary>
        /// finds the greedy tour starting from each city and keeps the best (valid) one
        /// </summary>
        /*// <returns>results array for GUI that contains three ints: cost of solution, time spent to find solution, number of solutions found during search (not counting initial BSSF estimate)</returns>
        public string[] greedySolveProblem()
        {
            string[] results = new string[3];
            Route = new ArrayList();
            Stopwatch timer = new Stopwatch();


            timer.Start();

            // TODO: Add your implementation for a greedy solver here.
            List<City> notVisited = new List<City>();

            foreach (City c in GetCities())
            {
                notVisited.Add(c);
            }

            TSPSolution best = null;
            double best_distance = double.PositiveInfinity;

            foreach (City c in GetCities())
            {
                bssf = null;
                List<City> visited = new List<City>();
                List<City> nVisited = new List<City>(notVisited);
                visited.Add(c);
                nVisited.Remove(c);
                recursiveGreedySolver(nVisited, visited);
                if (costOfBssf() < best_distance)
                {
                    best = bssf;
                    best_distance = costOfBssf();
                }
            }


            timer.Stop();

            results[COST] = costOfBssf().ToString();    // load results into array here, replacing these dummy values
            results[TIME] = timer.Elapsed.ToString();
            results[COUNT] = "1";

            return results;
        }
        //*/
        ///*
        public string[] greedySolveProblem()
        {
            string[] results = new string[3];

            Random rnd = new Random();
            int startInt = 0;
            int count = 0;
            City city = Cities[startInt];
            Stack<City> tour = new Stack<City>();
            double tourCost = double.PositiveInfinity;
            HashSet<City> noGood = new HashSet<City>();
            Stack<City> stack = new Stack<City>();
            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int j = 0; j < Cities.Length; j++)
            {
                city = Cities[j];
                while (stack.Count < Cities.Length + 1)
                {
                    stack.Push(city);
                    double min = double.PositiveInfinity;
                    int minCity = 0;
                    double minOld;
                    for (int i = 0; i < Cities.Length; i++)
                    {
                        if (!stack.Contains(Cities[i]))
                        {
                            if (!noGood.Contains(Cities[i]))
                            {
                                minOld = min;
                                min = Math.Min(min, city.costToGetTo(Cities[i]));
                                if (minOld != min)
                                    minCity = i;
                            }
                        }
                    }
                    if (double.IsPositiveInfinity(min))
                    {
                        if (stack.Count == Cities.Length)
                        {
                            if (!double.IsPositiveInfinity(stack.Peek().costToGetTo(Cities[j])))
                            {
                                stack.Push(Cities[j]);
                                count++;
                                break;
                            }
                            else
                            {
                                noGood.Add(stack.Pop());
                            }
                        }
                        continue;
                    }
                    city = Cities[minCity];
                    if (timer.ElapsedMilliseconds > time_limit)
                        break;
                }
                if (stack.Count == Cities.Length + 1)
                {
                    double thisCost = getTourCost(stack);
                    if (thisCost < tourCost)
                    {
                        tourCost = thisCost;
                        stack.Pop();
                        tour = new Stack<City>(stack);
                        stack.Clear();
                    }
                }
                if (timer.ElapsedMilliseconds > time_limit)
                    break;

            }

            timer.Stop();
            ArrayList tourArray = new ArrayList(tour.ToArray());
            bssf = new TSPSolution(tourArray);
            // TODO: Add your implementation for a greedy solver here.

            results[COST] = costOfBssf().ToString();    // load results into array here, replacing these dummy values
            results[TIME] = timer.Elapsed.ToString();
            results[COUNT] = count.ToString();

            return results;
        }

        private double getTourCost(Stack<City> stack)
        {
            Stack<City> cities = new Stack<City>(stack);
            cities = new Stack<City>(cities);
            City city1;
            City temp1;
            City temp2;
            double cost = 0;
            Stack<City> path = new Stack<City>(cities);
            cities = path;
            city1 = cities.Peek();
            temp1 = cities.Pop();
            while (cities.Count > 0)
            {
                temp2 = cities.Pop();
                if (double.IsPositiveInfinity(temp1.costToGetTo(temp2)))
                    return double.PositiveInfinity;
                cost += temp1.costToGetTo(temp2);
                temp1 = temp2;
            }
            return cost;
        }


        double CROSS_OVER_RATE = .6;
        Random CROSS_OVER_RANDOM = new Random(); //used to determine switching and which spot to switch with
        private TSPSolution cross_over(TSPSolution path1, TSPSolution path2)
        {
            if(CROSS_OVER_RANDOM.NextDouble() >= CROSS_OVER_RATE)
            {
                return path1;
            }


            int start = CROSS_OVER_RANDOM.Next(0, path1.Route.Count); // start of random subsequence of path1
            int end = CROSS_OVER_RANDOM.Next(start, path1.Route.Count); // end of random subsequence of path1

            ArrayList route = new ArrayList();
            ArrayList to_add = new ArrayList();

            for (int i = start; i < end; i++)                    // This loop add the selected subquence to a new rout
            {
                to_add.Add(path1.Route[i]);
            }

            foreach(City c in path2.Route)                      // Loops over path2's route
            {
                if (!route.Contains(c))                         // If the route does not contain the city, will add to the new route.
                {
                    route.Add(c);
                }
            }
            
            return new TSPSolution(route);                      // returns the newly created crossed over route.


        }

        private TSPSolution matchingCitiesCrossOver(TSPSolution path1, TSPSolution path2)
        {
            if (CROSS_OVER_RANDOM.NextDouble() >= CROSS_OVER_RATE)
            {
                return path1;
            }

            List<int> indexes = new List<int>();
            List<City> to_add = new List<City>();
            for (int i = 0; i < path1.Route.Count; i++)
            {
                if (path1.Route[i] != path2.Route[i])
                {
                    to_add.Add((City)path1.Route[i]);
                }
                else
                {
                    indexes.Add(i);
                }
            }

            int count = to_add.Count;
            for (int i = 0; i < to_add.Count; i++)
            {

                int index1 = CROSS_OVER_RANDOM.Next(0, count - 1);
                int index2 = CROSS_OVER_RANDOM.Next(0, count - 1);
                City temp = to_add[index1];
                to_add[index1] = to_add[index2];
                to_add[index2] = temp;

            }

            int j = 0;
            ArrayList route = new ArrayList();
            for (int i = 0; i < path1.Route.Count; i++)
            {
                if (indexes.Contains(i))
                {
                    route.Add(path1.Route[i]);
                }
                else
                {
                    route.Add(to_add[j]);
                    j++;
                }
            }


            return new TSPSolution(route);
        }

        private TSPSolution greedyCrossOver(TSPSolution path1, TSPSolution path2)
        {
            if (CROSS_OVER_RANDOM.NextDouble() >= CROSS_OVER_RATE)
            {
                return path1;
            }

            int index1 = CROSS_OVER_RANDOM.Next(0, path1.Route.Count - 1);
            ArrayList route = new ArrayList();
            route.Add(path1.Route[index1]);
            while (route.Count < path1.Route.Count)
            {
                City c = (City)route[route.Count - 1];
                int index_of_parent1 = path1.Route.IndexOf(c);
                int index_of_parent2 = path2.Route.IndexOf(c);

                int ic1, ic2, ic3, ic4;
                ic1 = index_of_parent1 - 1;
                ic2 = index_of_parent1 + 1;
                ic3 = index_of_parent2 - 1;
                ic4 = index_of_parent2 + 1;

                double best_distance = double.PositiveInfinity;
                City best_city = null;

                if (ic1 >= 0 && ((City)path1.Route[ic1]).costToGetTo(c) < best_distance)
                {
                    best_distance = ((City)path1.Route[ic1]).costToGetTo(c);
                    best_city = (City)path1.Route[ic1];
                }
                if (ic2 < path1.Route.Count && ((City)path1.Route[ic2]).costToGetTo(c) < best_distance)
                {
                    best_distance = ((City)path1.Route[ic2]).costToGetTo(c);
                    best_city = (City)path1.Route[ic2];
                }
                if (ic3 >= 0 && ((City)path2.Route[ic3]).costToGetTo(c) < best_distance)
                {
                    best_distance = ((City)path2.Route[ic3]).costToGetTo(c);
                    best_city = (City)path2.Route[ic3];
                }
                if (ic4 < path2.Route.Count && ((City)path2.Route[ic4]).costToGetTo(c) < best_distance)
                {
                    best_distance = ((City)path2.Route[ic4]).costToGetTo(c);
                    best_city = (City)path2.Route[ic4];
                }

                if (best_city == null || route.Contains(best_city))
                {
                    best_city = null;
                    while (best_city == null)
                    {
                        City temp_c = (City)path1.Route[CROSS_OVER_RANDOM.Next(0, path1.Route.Count)];
                        if (!route.Contains(temp_c))
                        {
                            best_city = temp_c;
                        }
                    }
                }

                route.Add(best_city);


            }

            return new TSPSolution(route);
        }

        double MUTATION_RATE = .2;
        double rate = .05;
        Random MUTATION_RANDOM = new Random(); //used to determine switching and which spot to switch with
        private TSPSolution mutate(TSPSolution path)
        {
            
            TSPSolution newPath = new TSPSolution(path.Route);

            if (MUTATION_RANDOM.NextDouble() < MUTATION_RATE)
                return newPath;


            
            int index1 = MUTATION_RANDOM.Next(0, path.Route.Count - 1);
            int index2 = MUTATION_RANDOM.Next(0, path.Route.Count - 1);

            if(index2 < index1)
            {
                List<int> indexes = new List<int>();
                for (int i = 0; i < index2; i++) indexes.Add(i);
                for (int i = index1; i < newPath.Route.Count; i++) indexes.Add(i);

                for(int i = 0; i < indexes.Count; i++)
                {
                    int temp = MUTATION_RANDOM.Next(0, indexes.Count);
                    City temp_c = (City)newPath.Route[indexes[i]];
                    newPath.Route[indexes[i]] = newPath.Route[indexes[temp]];
                    newPath.Route[indexes[temp]] = temp_c;
                    //indexes.Remove(indexes[temp]);
                }

            }
            else{
                for (int i = index1; i < index2; i++)
                {
                    int temp = MUTATION_RANDOM.Next(index1, index2);
                    City temp_c = (City)newPath.Route[i];
                    newPath.Route[i] = newPath.Route[temp];
                    newPath.Route[temp] = temp_c;
                }
            }
            

            /*
            int i = MUTATION_RANDOM.Next(0, path.Route.Count - 1);
            int next = MUTATION_RANDOM.Next(0, path.Route.Count - 1);
            City old_city = (City)newPath.Route[i];                 // some terrible code to swap elements in an array
            newPath.Route[i] = newPath.Route[next];
            newPath.Route[next] = old_city;
            */
            /*
            for (int i = 0; i < path.Route.Count; i++)
            {
                if(MUTATION_RANDOM.NextDouble() < rate)            // determines if the current city will be swapped with another city
                {
                    
                }
            }
            */

            return newPath;    
        }

        private List<TSPSolution> mutateList(TSPSolution path)
        {


            List<TSPSolution> paths = new List<TSPSolution>();
            
            for (int i = 0; i < path.Route.Count; i++)
            {
                if(MUTATION_RANDOM.NextDouble() < rate)            // determines if the current city will be swapped with another city
                {
                    TSPSolution newPath = new TSPSolution(path.Route);
                    int next = MUTATION_RANDOM.Next(0, path.Route.Count - 1);
                    City old_city = (City)newPath.Route[i];                 // some terrible code to swap elements in an array
                    newPath.Route[i] = newPath.Route[next];
                    newPath.Route[next] = old_city;
                    paths.Add(newPath);
                }
            }
            
            return paths;
        }

        //creates a random path with no prior information
        Random rand = new Random();
        private TSPSolution randomPath()
        {
            int i, swap, temp, count = 0;
            int[] perm = new int[Cities.Length];
            Route = new ArrayList();
            
            TSPSolution path = null;

            do
            {
                for (i = 0; i < perm.Length; i++)                                 // create a random permutation template
                    perm[i] = i;
                for (i = 0; i < perm.Length; i++)
                {
                    swap = i;
                    while (swap == i)
                        swap = rand.Next(0, Cities.Length);
                    temp = perm[i];
                    perm[i] = perm[swap];
                    perm[swap] = temp;
                }
                Route.Clear();
                for (i = 0; i < Cities.Length; i++)                            // Now build the route using the random permutation 
                {
                    Route.Add(Cities[perm[i]]);
                }
                path = new TSPSolution(Route);
                count++;
            } while (path.costOfRoute() == double.PositiveInfinity);                // until a valid route is found

            return path;
        }

        Random parent_random = new Random();
        private TSPSolution selectParent(List<TSPSolution> solutions, int parent)
        {

            //return solutions[parent];
            if(parent_random.NextDouble() >= .8)
            {
                return solutions[parent];
            }
            return solutions[parent_random.Next(0, solutions.Count)];
            

            //return solutions[parent_random.Next(0, solutions.Count - 1)];
        }

        private TSPSolution tournamentSelction(List<TSPSolution> pop, int k)
        {
            TSPSolution best = null;
            for(int i = 0; i < k; i++)
            {
                TSPSolution individual = pop[rand.Next(pop.Count - 1)];
                if (best == null || individual.costOfRoute() < best.costOfRoute()) best = individual;
            }
            return best;
        }

        //MODIFY THESE VALUES TO CHANGE POPULATION SIZE (per generation) AND NUMBER OF GENERATIONS (does not include inital population)
        int population_size = 200;
        int num_generations = 1000;
        int max_list_size = 5;
        public string[] fancySolveProblem(TSP.mainform main)
        {
            string[] results = new string[3];
            Stopwatch timer = new Stopwatch();

            

            greedySolveProblem();
            double greedy_cost = costOfBssf();


            timer.Start();

            List<TSPSolution> best_list = new List<TSPSolution>();
            List<TSPSolution> solutions = new List<TSPSolution>();

            

            for (int i = 0; i < population_size; i++)
            {
                TSPSolution path = randomPath();
                solutions.Add(randomPath());
            }

            solutions.Sort((c1, c2) => (c1.costOfRoute().CompareTo(c2.costOfRoute())));
            solutions.Add(bssf);
            //solutions.Add(bssf);
            //solutions.Add(bssf);
            //solutions.Add(bssf);
            //solutions.Add(bssf);

        
            bssf = solutions[0];
            main.setText((greedy_cost / costOfBssf()).ToString());
            main.Invalidate();
            main.Refresh();

            //THIS DOES MUTATION AND CROSSOVER

            int completed_generations = 0;
            for (int i = 0; i < num_generations; i++)
            {
               

                solutions.Sort((s1, s2) => (s1.costOfRoute().CompareTo(s2.costOfRoute())));
                if(solutions.Count > population_size)
                {
                    solutions.RemoveRange(population_size-10, solutions.Count - population_size);
                }
                best_list = solutions.GetRange(0, max_list_size);
                for(int j = 0; j < 1; j++)
                {
                    best_list.Add(solutions[rand.Next(0, solutions.Count - 1)]);
                }
                best_list.Add(randomPath());

                
                for (int j = 0; j < population_size; j++)
                {
                    TSPSolution parent_1 = tournamentSelction(solutions, 2);
                    TSPSolution parent_2 = tournamentSelction(solutions, 2);
                    TSPSolution path;


                    path = greedyCrossOver(parent_1, parent_2);
                    path = mutate(path);
                    solutions.Add(path);
                    if (path.costOfRoute() < bssf.costOfRoute())
                    {
                        bssf = path;
                        main.setText((greedy_cost / costOfBssf()).ToString());
                        main.Invalidate();
                        main.Refresh();
                    }


                    /*
                    List<TSPSolution> paths = mutateList(path);
                    foreach(TSPSolution sol in paths)
                    {
                        if (sol.costOfRoute() < bssf.costOfRoute())
                        {
                            bssf = sol;
                            main.setText((greedy_cost / costOfBssf()).ToString());
                            main.Invalidate();
                            main.Refresh();
                        }
                    }
                    solutions.AddRange(paths);
                    */


                    if (timer.Elapsed.TotalMilliseconds >= time_limit)
                    {
                        break;
                    }
                    

                }
                if (timer.Elapsed.TotalMilliseconds >= time_limit)
                {
                    break;
                }
                completed_generations++;
            }

            timer.Stop();

            results[COST] = costOfBssf().ToString();    // load results into array here, replacing these dummy values
            results[TIME] = timer.Elapsed.ToString();
            results[COUNT] = completed_generations.ToString() ;

            return results;
        }
        #endregion
    }

}
