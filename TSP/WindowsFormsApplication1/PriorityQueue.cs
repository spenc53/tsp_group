﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TSP
{
    class ArrayPriorityQueue
    {
        List<State> queue;

        public ArrayPriorityQueue()
        {
            queue = new List<State>();
        }

        //Time Complexity O(1)
        public void insert(State s)
        {
            queue.Add(s);
        }

        //Time Complexity O(n)
        public State deleteMin()
        {
            //State c = queue[queue.Count - 1];
            //queue.RemoveAt(queue.Count - 1);
            //return c;
            
            State node = null;
            double value = double.PositiveInfinity;

            //iterate through queue to find the smallest value
            foreach (State state in queue)
            {
                if (state == null) continue;
                double testValue = state.value();
                //check to see if the value of an element in the list is better
                //than my current best value
                if (testValue < value)
                {
                    value = testValue;
                    node = state;
                }
            }

            if (node == null) return node;
            queue.Remove(node);
            return node;
            
        }

        //Time Complexity O(1)
        public int size()
        {
            return queue.Count;
        }

    }

}

