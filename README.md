# TSP GROUP PROJECT (GENETIC ALGORITHM)

## WHAT IS DONE
 - mutation
 - crossover
 - timer
 - displaying best path
 - Greedy is now iterative

## WHAT NEEDS TO BE DONE
 - Count (I think how many times the bssf is updated aka how many times we find something better, I am unsure of how to count this.)
 
 - Write-up(currently working on it -Zack)
 - Presentation

## Overview of functions
### MUTATE(TSPSolution path)
This functions takes in a path and mutates it. It iterates through the current route and then draws a random number between 0 and 1. If the number is less than MUTATION_RATE, then it swaps it with a random element in the list.

### CROSS-OVER(TSPSolution path1, TSPSolution path2)
This functions takes in two paths (probably the best two paths so far) and then cross them over. First, it generates a random number and sees if it is more than CROSS_OVER_RATE, if it is, it will return either path1 or path2 (each with 50% probability). If the random number is less than CROSS_OVER_RATE, then it selects a random subsequence from path1 and then adds the rest of path2. When combining, any cities in path2 will be skipped if they are already in the random subsequence from path1.


## Quick Notes
We can change the mutation rate, cross-over rate, population size of a generation,and the number of generations to be ran.
With 500 size of population and 500 generations, on a sample of 50 cities, it took about 10 seconds to run.

